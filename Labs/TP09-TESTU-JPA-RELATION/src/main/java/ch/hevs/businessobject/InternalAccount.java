package ch.hevs.businessobject;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class InternalAccount {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	private String number;	
	private String description;
	private float saldo;

	// add relations
	@OneToMany(cascade = CascadeType.ALL)
	private List<Operation> operations;

	@ManyToMany(mappedBy = "internalAccounts", cascade = CascadeType.ALL)
	private List<Client> owners;

	public List<Operation> getOperations() {
		return operations;
	}

	public void setOperations(List<Operation> operations) {
		this.operations = operations;
	}

	public List<Client> getOwners() {
		return owners;
	}

	public void setOwners(List<Client> owners) {
		this.owners = owners;
	}

	// id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	// number
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	
	// description
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	// saldo
	public float getSaldo() {
		return saldo;
	}
	public void setSolde(float saldo) {
		this.saldo = saldo;
	}


	// constructors
	public InternalAccount() {
		this.owners = new ArrayList<>();
		this.operations = new ArrayList<>();
	}
	public InternalAccount(String number, String description, float saldo) {
		this.description = description;
		this.number = number;
		this.saldo = saldo;
		this.owners = new ArrayList<>();
		this.operations = new ArrayList<>();
	}

	// Helper methods
	public void addOwner(Client client) {
		owners.add(client);
	}
	public void addOperation(Operation o) {
		operations.add(o);
	}
}
