package ch.hevs.businessobject;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Client {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	private String description;
	private String lastname;
	private String firstname;

	// add relations
	@ManyToMany(cascade = CascadeType.ALL)
	private List<InternalAccount> internalAccounts;

	@OneToMany(cascade = CascadeType.ALL)
	private List<ExternalAccount> externalAccounts;

	@OneToOne(cascade = CascadeType.ALL)
	private Address address;

	@ManyToOne(cascade = CascadeType.ALL)
	private Agency agency;

	public List<InternalAccount> getInternalAccounts() {
		return internalAccounts;
	}

	public void setInternalAccounts(List<InternalAccount> internalAccounts) {
		this.internalAccounts = internalAccounts;
	}

	public List<ExternalAccount> getExternalAccounts() {
		return externalAccounts;
	}

	public void setExternalAccounts(List<ExternalAccount> externalAccounts) {
		this.externalAccounts = externalAccounts;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Agency getAgency() {
		return agency;
	}

	public void setAgency(Agency agency) {
		this.agency = agency;
	}

	// id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	// description
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	// lastname
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	// firstname
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	// constructors
	public Client() {
		this.internalAccounts = new ArrayList<>();
		this.externalAccounts = new ArrayList<>();
	}
	public Client(String lastname, String firstname, String description) {
		this.description = description;
		this.lastname = lastname;
		this.firstname = firstname;
		this.internalAccounts = new ArrayList<>();
		this.externalAccounts = new ArrayList<>();
	}

	// helper methods
	public void addInternalAccount(InternalAccount c) {
		internalAccounts.add(c);
		c.addOwner(this);
	}
	public void addExternalAccount(ExternalAccount c) {
		externalAccounts.add(c);
	}
}
