package ch.hevs.businessobject;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Agency {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	// add relations
	@OneToMany(mappedBy = "agency", cascade = CascadeType.ALL)
	private List<Client> clients;

	@OneToOne(cascade = CascadeType.ALL)
	private Address address;

	@OneToMany(mappedBy = "agency", cascade = CascadeType.ALL)
	private List<Banker> bankers;

	public List<Client> getClients() {
		return clients;
	}

	public void setClients(List<Client> clients) {
		this.clients = clients;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public List<Banker> getBankers() {
		return bankers;
	}

	public void setBankers(List<Banker> bankers) {
		this.bankers = bankers;
	}

	// id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	// constructors
	public Agency() {
		this.clients = new ArrayList<>();
		this.bankers = new ArrayList<>();
	}

	// Helper methods
	public void addClient(Client c) {
		clients.add(c);
		c.setAgency(this);
	}
	public void addBanker(Banker b) {
		bankers.add(b);
		b.setAgency(this);
	}
}
