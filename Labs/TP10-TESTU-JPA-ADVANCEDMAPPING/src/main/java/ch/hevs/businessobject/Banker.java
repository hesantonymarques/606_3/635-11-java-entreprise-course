package ch.hevs.businessobject;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class Banker extends Person{

	private String email;

	// relations
	@ManyToOne(cascade = CascadeType.ALL)
	private Agency agency;

	// constructors
	public Banker() {
	}
	public Banker(String lastname, String firstname, String email) {
		this.email = email;
	}

	// GETTER AND SETTER
	// email
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	// agency
	public Agency getAgency() {
		return agency;
	}
	public void setAgency(Agency agency) {
		this.agency = agency;
	}
}
