package ch.hevs.businessobject;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Client extends Person{

	private String description;

	// relations
	@ManyToMany(cascade = CascadeType.ALL)
	private List<InternalAccount> internalAccounts;

	@OneToMany(cascade = CascadeType.ALL)
	private List<ExternalAccount> externalAccounts;

	@ManyToOne(cascade = CascadeType.ALL)
	private Agency agency;

	@Embedded
	private Address address;

	// constructors
	public Client() {
		// instantiate lists
		this.internalAccounts = new ArrayList<>();
		this.externalAccounts = new ArrayList<>();
	}
	public Client(String lastname, String firstname, String description) {
		super(lastname, firstname);
		this.description = description;

		// instantiate lists
		this.internalAccounts = new ArrayList<>();
		this.externalAccounts = new ArrayList<>();
	}

	// helper methods
	public void addInternalAccount(InternalAccount c) {
		internalAccounts.add(c);
		c.addOwner(this);
	}
	public void addExternalAccount(ExternalAccount c) {
		externalAccounts.add(c);
	}

	// GETTER AND SETTER
	// description
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	// internal accounts
	public List<InternalAccount> getInternalAccounts() {
		return internalAccounts;
	}
	public void setInternalAccounts(List<InternalAccount> internalAccounts) {
		this.internalAccounts = internalAccounts;
	}

	// external accounts
	public List<ExternalAccount> getExternalAccounts() {
		return externalAccounts;
	}
	public void setExternalAccounts(List<ExternalAccount> externalAccounts) {
		this.externalAccounts = externalAccounts;
	}

	// agency
	public Agency getAgency() {
		return agency;
	}
	public void setAgency(Agency agency) {
		this.agency = agency;
	}

	// address
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
}
