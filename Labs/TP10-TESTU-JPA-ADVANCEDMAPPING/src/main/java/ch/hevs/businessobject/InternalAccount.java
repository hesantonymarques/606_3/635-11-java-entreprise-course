package ch.hevs.businessobject;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
public class InternalAccount extends Account{
	
	private String number;
	private float saldo;

	// relations
	@OneToMany(cascade = CascadeType.ALL)
	private List<Operation> operations;

	@ManyToMany(mappedBy = "internalAccounts", cascade = CascadeType.ALL)
	private List<Client> owners;

	// constructors
	public InternalAccount() {
		// instantiate lists
		this.owners = new ArrayList<>();
		this.operations = new ArrayList<>();
	}
	public InternalAccount(String number, String description, float saldo) {
		super(description);
		this.number = number;
		this.saldo = saldo;

		// instantiate lists
		this.owners = new ArrayList<>();
		this.operations = new ArrayList<>();
	}

	// helper methods
	public void addOwner(Client client) {
		owners.add(client);
	}
	public void addOperation(Operation o) {
		operations.add(o);
	}

	// GETTER AND SETTER
	// number
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	
	// saldo
	public float getSaldo() {
		return saldo;
	}
	public void setSaldo(float saldo) {
		this.saldo = saldo;
	}
}
