package ch.hevs.businessobject;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Agency {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	// relations
	@OneToMany(mappedBy = "agency", cascade = CascadeType.ALL)
	private List<Client> clients;

	@OneToMany(mappedBy = "agency", cascade = CascadeType.ALL)
	private List<Banker> employees;

	@Embedded private Address address;

	// constructors
	public Agency() {
		this.clients = new ArrayList<>();
		this.employees = new ArrayList<>();
	}

	// helper methods
	public void addClient(Client c) {
		clients.add(c);
		c.setAgency(this);
	}
	public void addEmployee(Banker b) {
		employees.add(b);
		b.setAgency(this);
	}

	// GETTER AND SETTER
	// id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	// clients
	public List<Client> getClients() {
		return clients;
	}
	public void setClients(List<Client> clients) {
		this.clients = clients;
	}

	// employees (bankers)
	public List<Banker> getEmployees() {
		return employees;
	}
	public void setEmployees(List<Banker> employees) {
		this.employees = employees;
	}

	// address
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
}
