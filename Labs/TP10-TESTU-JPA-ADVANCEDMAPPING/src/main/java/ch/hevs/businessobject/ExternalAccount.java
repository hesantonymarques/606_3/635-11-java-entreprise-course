package ch.hevs.businessobject;

import javax.persistence.Entity;

@Entity
public class ExternalAccount extends Account{

	private String socialNumber;

	// constructors
	public ExternalAccount() {
	}
	public ExternalAccount(String socialNumber, String description) {
		this.socialNumber = socialNumber;
	}

	// GETTER AND SETTER
	// socialNumber
	public String getSocialNumber() {
		return socialNumber;
	}
	public void setSocialNumber(String socialNumber) {
		this.socialNumber = socialNumber;
	}
}
