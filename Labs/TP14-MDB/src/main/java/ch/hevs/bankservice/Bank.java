package ch.hevs.bankservice;

import java.util.List;

import javax.ejb.Local;
import javax.jms.JMSException;

import ch.hevs.businessobject.Client;
import ch.hevs.businessobject.Account;
import ch.hevs.exception.BankException;


@Local
public interface Bank {

	public List<Client> getClients() throws BankException;

	public Client getClientByLastname(String lastname) throws BankException;

	public Client getClientById(long id) throws BankException;

	public List<Account> getAccountFromClient(long Id) throws BankException;

	public String transfer(Account source, Account destination, int amount)
			throws BankException,JMSException;

	public Account getAccount(String accountDescription, String lastnameOwner);
	
	public List<Account> getAccountListFromClientLastname(String lastname);

}
