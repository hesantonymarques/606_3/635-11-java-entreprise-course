package ch.hevs.bankservice;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import ch.hevs.businessobject.Account;
import ch.hevs.businessobject.Client;
import ch.hevs.exception.BankException;


@Stateful
public class BankBean implements Bank {
	
	@PersistenceContext(name = "BankPU", type = PersistenceContextType.EXTENDED)
	EntityManager em;
	
	@Resource(lookup = "java:jboss/DefaultJMSConnectionFactory")
	QueueConnectionFactory factory;

	@Resource(lookup = "java:/jms/queue/DLQ")
	Queue messageQueue;

	

	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public String transfer(Account source, Account destination, int amount)
			throws BankException,JMSException {

		source.debit(amount);
		destination.credit(amount);
		
		String transactionResult="Success!";
		
		sendMyMessage(source.getOwner(), destination.getOwner(), amount);
		System.out.println("From BankBean: the message has just been sent to the Message-Driven Bean (MDB)");
		
		return transactionResult;
	}

	private void sendMyMessage(Client source, Client dest, int montant) throws JMSException {
		
		String msg = "A transfer has just been made from " + source.getLastname()
				+ " to " + dest.getLastname() + ". Amount:" + montant;
		
		//System.out.println("From BankBean: "+msg);

		try {
			
			QueueConnection myQueueConnection = factory.createQueueConnection();
			QueueSession myQueueSession = myQueueConnection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			QueueSender myQueueSender = myQueueSession.createSender(messageQueue);
			TextMessage myMessage = myQueueSession.createTextMessage();
			myMessage.setText(msg);
			myQueueSender.send(myMessage);
			
		} catch (JMSException e) {

			e.printStackTrace();
		}
	}

	public List<Client> getClients() throws BankException {
		return em.createQuery("from Client c").getResultList();
	}

	public Client getClientByLastname(String lastname) throws BankException {
		Client result = null;
		List<Client> clients = getClients();
		for (Client c : clients) {
			if (c.getLastname().equals(lastname)) {
				result = c;
				break;
			}
		}
		return result;
	}

	public Client getClientById(long id) throws BankException {

		return em.find(Client.class, id);
	}

	public List<Account> getAccountFromClient(long id) throws BankException {
		return em.createQuery("FROM Account c WHERE c.owner.id=:id").setParameter("id",	id).getResultList();
	}

	public Account getAccount(String accountDescription, String lastnameOwner) {
		Query query = em.createQuery("FROM Account a WHERE a.description=:description AND a.owner.lastname=:lastname");
		query.setParameter("description", accountDescription);
		query.setParameter("lastname", lastnameOwner);
		
		return (Account) query.getSingleResult();
	}
	
	public List<Account> getAccountListFromClientLastname(String lastname) {
		return (List<Account>) em.createQuery("SELECT c.accounts FROM Client c where c.lastname=:lastname").setParameter("lastname", lastname).getResultList();
	}

}
