package ch.hevs.bankservice;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/jms/queue/DLQ")
})
public class MyMessageListener implements MessageListener {

	public void onMessage(Message msg) {
		try {
			TextMessage text = (TextMessage) msg;
			Thread.sleep(10000); // wait 10 seconds to simulate long data processing
			System.out.println("From MyMessageListener: "+text.getText());
		} catch (JMSException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
