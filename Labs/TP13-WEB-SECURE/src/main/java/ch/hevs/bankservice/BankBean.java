package ch.hevs.bankservice;

import ch.hevs.businessobject.Account;
import ch.hevs.businessobject.Client;

import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import java.util.List;

@Stateful
@RolesAllowed(value = { "client", "banker" })
public class BankBean implements Bank {
	
	@PersistenceContext(name = "BankPU", type = PersistenceContextType.EXTENDED)
	private EntityManager em;

	@Resource
	private SessionContext ctx;

	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public String transfer(Account srcAccount, Account destAccount, int amount) {
		String transactionResult = "";

		System.out.println(ctx.getCallerPrincipal().getName() + "is looking for a transfer with amount of CHF " + amount + ".");

		em.persist(srcAccount);
		em.persist(destAccount);
		srcAccount.debit(amount);
		destAccount.credit(amount);

		if(!ctx.isCallerInRole("banker") && amount>100){
			System.out.println("You can't make this transfer!");

			ctx.setRollbackOnly();

			transactionResult="NOT AUTHORIZED !";
		} else {
			System.out.println("You can make this transfer!");

			transactionResult="Success!";
		}

		return transactionResult;

	}

	public Account getAccount(String accountDescription, String lastnameOwner) {
		Query query = em.createQuery("FROM Account a WHERE a.description=:description AND a.owner.lastname=:lastname");
		query.setParameter("description", accountDescription);
		query.setParameter("lastname", lastnameOwner);
		
		return (Account) query.getSingleResult();
	}
	
	public List<Account> getAccountListFromClientLastname(String lastname) {
		return (List<Account>) em.createQuery("SELECT c.accounts FROM Client c where c.lastname=:lastname").setParameter("lastname", lastname).getResultList();
	}

	public List<Client> getClients() {
		return em.createQuery("FROM Client").getResultList();
	}
	
	public Client getClient(long clientid) {
		return (Client) em.createQuery("FROM Client c where c.id=:id").setParameter("id", clientid).getSingleResult();
	}
}
