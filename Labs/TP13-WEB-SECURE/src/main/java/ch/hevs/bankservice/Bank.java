package ch.hevs.bankservice;

import ch.hevs.businessobject.Account;
import ch.hevs.businessobject.Client;

import javax.ejb.Local;
import java.util.List;

@Local
public interface Bank {
	
	public String transfer(Account compteSrc, Account compteDest, int montant) throws Exception;

	public Account getAccount(String accountDescription, String lastnameOwner);
	
	public List<Account> getAccountListFromClientLastname(String lastname);

	public List<Client> getClients();

	public Client getClient(long clientid);
}
