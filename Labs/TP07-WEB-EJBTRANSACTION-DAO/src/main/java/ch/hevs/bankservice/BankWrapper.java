package ch.hevs.bankservice;

import ch.hevs.businessobject.Account;
import ch.hevs.businessobject.Client;
import ch.hevs.exception.BankException;

import javax.ejb.Local;
import java.util.List;

@Local
public interface BankWrapper {
    void transferAndCreateClient(Account source, Account destination, int amount) throws BankException;
    public Client getClientByName(String name);
    public List<Client> getClients();
}
