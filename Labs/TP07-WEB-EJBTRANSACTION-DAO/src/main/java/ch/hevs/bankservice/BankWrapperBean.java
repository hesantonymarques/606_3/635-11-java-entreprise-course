package ch.hevs.bankservice;

import ch.hevs.businessobject.Account;
import ch.hevs.businessobject.Client;
import ch.hevs.exception.BankException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class BankWrapperBean implements BankWrapper {

    @EJB
    private Bank bank;

    @Override
    public void transferAndCreateClient(Account source, Account destination, int amount) throws BankException {
        bank.createClient();
        bank.transfer(source, destination, amount);
    }

    public Client getClientByName(String name){
        return bank.getClientByName(name);
    }
    public List<Client> getClients() {
        return bank.getClients();
    }
}
